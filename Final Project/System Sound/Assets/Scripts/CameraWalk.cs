﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWalk : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.acceleration.z <= -0.2f)
        {
            transform.position = transform.position + Camera.main.transform.forward * 3 * Time.deltaTime;
        }
	}
}
