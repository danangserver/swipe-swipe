using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour {

    [SerializeField]
    private GameManager manager;
    [SerializeField]
    private ParticleSystem particle;

	public float speed = 0.5f;

    private bool gyroEnabled;
    private Gyroscope gyro;

    public Vector3 startPosition;
    public Text text;

    //variable keyboard move
    public float speedMove = 10f;
    public float rotateSpeed = 10f;

    private Vector3 movement;
    private float rotation;


    void Start()
    {
        gyroEnabled = EnableGyro();
        startPosition = transform.position;
    }

	void Update ()
	{
        StickControl();
        if (manager.isPlay)
        {
            if (gyroEnabled)
            {
                text.text = gyroEnabled.ToString() ;
                //transform.Rotate(0, -Input.gyro.rotationRateUnbiased.y, 0);
            }
        }

        if (EnableAccel())
        {
            transform.Translate(0, 0, -Input.acceleration.z * speed);
        }

        //move with keyboard

        //movement.z = Input.GetAxis("Vertical") * speedMove * Time.deltaTime;
        //Debug.Log(movement.z);
        //rotation = Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime;
        //Debug.Log(rotation);
        //transform.Translate(movement, Space.Self);
        //transform.Rotate(0f, rotation, 0f);
    }

    void FixUpdate()
    {
        //move with keyboard

    }

    void StickControl()
    {
        if (Input.GetAxis("Vertical") > 0.3f)
        {
            transform.Translate(new Vector3(0, 0, 5 * Time.deltaTime));
        }else if(Input.GetAxis("Vertical") < -0.3f)
        {
            transform.Translate(new Vector3(0, 0, -5 * Time.deltaTime));
        }
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;

            return true;
        }
        return false;
    }

    private bool EnableAccel()
    {
        if (SystemInfo.supportsAccelerometer)
        {
            return true;
        }
        return false;
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Point")
        {
            manager.GetComponent<GameManager>().score += 1;
            manager.GetComponent<GameManager>().time += 90;
            manager.GetComponent<GameManager>().textScore.text = manager.GetComponent<GameManager>().score.ToString() + "/5";
            Instantiate(particle, other.gameObject.transform.position, Quaternion.identity);
            Destroy(other.gameObject, 1);
        }
    }
}
