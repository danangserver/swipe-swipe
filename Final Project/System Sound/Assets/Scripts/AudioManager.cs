﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager instance;

    public Sound[] sounds;

	void Awake () {

        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

		foreach (Sound s in sounds)
        {
            s.source = gameObject.GetComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.spatialBlend = s.blend;
            s.source.spread = s.spread;
            s.source.minDistance = s.minDistance;
            s.source.maxDistance = s.maxDistance;

            if (s.linear)
                s.source.rolloffMode = AudioRolloffMode.Linear;
            else
                s.source.rolloffMode = AudioRolloffMode.Logarithmic;
        }
	}
	
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.Log("Sound : " + name + " not found !");
            return;
        }
        s.source.Play();
    }
}
