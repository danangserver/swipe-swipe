﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {

    public InputField ID, IP;

    private string nama, ip;

    // Use this for initialization
    void Start () {
        nama = ID.text;
        ip = IP.text;
        /*
        try
        {
            socketForServer = new TcpClient(ip, 8000);
        }
        catch
        {
            Console.WriteLine(
            "Failed to connect to server at {0}:999", "localhost");
            return;
        }
        Thread th = new Thread(new ThreadStart(TransferData));
        th.Start();
        */
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Submit()
    {
        PlayerPrefs.SetString("nama", nama);
        PlayerPrefs.SetString("ip", ip);
        SceneManager.LoadScene(3);
    }
}
