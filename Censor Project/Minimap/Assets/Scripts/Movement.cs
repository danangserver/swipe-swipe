using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    [SerializeField]
    private GameManager manager;
    [SerializeField]
    private ParticleSystem particle;

	public float speed = 0.5f;

    private bool gyroEnabled;
    private Gyroscope gyro;

    public Vector3 startPosition;

    void Start()
    {
        gyroEnabled = EnableGyro();
        startPosition = transform.position;
    }

	void Update ()
	{
        if (manager.isPlay)
        {
            if (gyroEnabled)
            {
                transform.Rotate(0, -Input.gyro.rotationRateUnbiased.y, 0);
            }
            transform.Translate(0, 0, -Input.acceleration.z * speed);
        }
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;

            return true;
        }
        return false;
    }
	
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Point")
        {
            manager.GetComponent<GameManager>().score += 1;
            manager.GetComponent<GameManager>().time += 90;
            manager.GetComponent<GameManager>().textScore.text = manager.GetComponent<GameManager>().score.ToString() + "/5";
            Instantiate(particle, other.gameObject.transform.position, Quaternion.identity);
            Destroy(other.gameObject, 1);
        }
    }
}
