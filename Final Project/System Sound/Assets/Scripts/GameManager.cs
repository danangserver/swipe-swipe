﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public enum State
    {
        Start,
        Playing,
        End
    }

    public State state;
    public Text textScore, timeText, questText;

    public float score = 0;
    public float time = 300;
    public bool isPlay = false;

    public Vector3 accel;

    void Start () {
        state = State.Start;
        //FindObjectOfType<AudioManager>().Play("bgm");
    }
	
	void Update () {
        questText.text = accel.z.ToString();
        if (state == State.Start)
        {
            if (Input.GetMouseButtonDown(0))
            {
                questText.text = "Colllect all yellow box and find out the way before times over !";
                //GameObject.Find("PanelQuest").GetComponent<Transform>().localScale = new Vector3(0,0,0);
                //StartCoroutine(destroyQuest());
                state = State.Playing;
            }
        }
        else if(state == State.Playing)
        {
            accel = Vector3.zero;
            accel.z = -Input.acceleration.z;
            if (!isPlay)
                isPlay = true;

            if (time > 0)
            {
                time -= Time.deltaTime;
                timeText.text = "TimeLeft : " + Mathf.Round(time).ToString();
            }
            else
            {
                questText.text = "You Lose !!";
                state = State.End;
            }

            if (score >= 5 && time > 0)
            {
                questText.text = "Congratulation !! You Win !!";
                state = State.End;
            }

        }else if(state == State.End)
        {
            if(isPlay)
                isPlay = false;

            if (Input.GetMouseButtonDown(0))
                SceneManager.LoadScene(0);

            GameObject.Find("PanelQuest").GetComponent<Transform>().localScale = new Vector3(1, 1, 1);
        }
	}

    IEnumerator destroyQuest()
    {
        yield return new WaitForSeconds(5);
        GameObject.Find("PanelQuest").GetComponent<Transform>().localScale = new Vector3(0, 0, 0);
    }

    public void backToStartPosition()
    {
        GameObject.Find("Pacmanlo").transform.position = GameObject.Find("Pacmanlo").GetComponent<Movement>().startPosition;
    }
}
