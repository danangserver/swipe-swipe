﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound {

    public string name;

    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume;
    [Range(.1f, 3f)]
    public float pitch;
    [Range(0f, 360f)]
    public float spread;
    [Range(0f, 500f)]
    public float minDistance;
    [Range(0f, 500f)]
    public float maxDistance;
    [Tooltip("3D or 2D Sound")]
    [Range(0f, 1f)]
    public float blend;

    public bool loop;
    public bool linear;

    [HideInInspector]
    public AudioSource source;
}
