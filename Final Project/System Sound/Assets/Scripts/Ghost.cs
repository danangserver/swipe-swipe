﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour {

    public GameObject player;
    public float speed = 0.5f;
    bool move = false;
    Vector3 startPosition;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        if (move)
        {
            //gameObject.transform.LookAt(player.transform);
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, player.transform.position, speed * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, startPosition, speed * Time.deltaTime);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            move = true;
            Debug.Log("Collider");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            move = false;
            Debug.Log("Collider");
        }
    }
}
