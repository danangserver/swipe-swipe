Menggunakan 
- 3 sensor : Acelerometer, Gyrometer, Magnetic
- 1 alat tambahan : Virtual Reality
- tambahan fitur 3D sound

Game
Game bergenre maze horror, dimana pemain harus menemukan jalan keluar dari sebuah labirin yang sangat gelap. untuk mencari jalan keluar pemain hanya dibekali oleh satu cahaya obor dan juga suara suara yang dapat mengarahkan kejalan keluar sebelum obor habis.
control player menggunakan acelerometer untuk maju, gyrometer untuk belok kanan dan kiri, dan Magnetic menjadi petunjuk arah untuk menemukan jalan keluar. alat virtual reality digunakan untuk membuat keadaan menjadi terlihat nyata dan juga menambah pengalaman dalam bermain.