﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour {

    private float m_lastMagneticHeading = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Input.location.Start();
        Input.compass.enabled = true;
        //transform.localRotation = Quaternion.Euler(0, 0, -Input.compass.magneticHeading);
        float currentMagneticHeading = (float)Math.Round(Input.compass.magneticHeading, 2);
        if (m_lastMagneticHeading < currentMagneticHeading - 0.5f || m_lastMagneticHeading > currentMagneticHeading + 0.5f)
        {
            m_lastMagneticHeading = currentMagneticHeading;
            transform.localRotation = Quaternion.Euler(0, 0, m_lastMagneticHeading);
        }
    }
}
