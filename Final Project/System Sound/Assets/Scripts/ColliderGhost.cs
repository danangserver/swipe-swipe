﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderGhost : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Pacman")
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().time = 0;
        }
    }
}
